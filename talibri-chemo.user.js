// ==UserScript==
// @name         Talibri Chemo
// @namespace    http://tampermonkey.net/
// @version      1.8.1
// @description  Remove some of the Cancer
// @author       Dan Campbell
// @match        *://talibri.com/*
// @grant       GM_setValue
// @grant       GM_getValue
// ==/UserScript==

var override = {};
var defaults = {};
var chemo = {};
var savedCfg = {};

/******************************************************************
* This script uses local storage to record your settings. The
* script will first look for saved values to the config and replace
* with default values if not found. It will then look for anything
* set as an "override" value. The winning value will then be saved.
* Please only modify the values of the "override" object and not
* "default"! If you would like to reset a value to default, change
* its value to 'default'.
******************************************************************/

/*      BEGIN EDITING      */

/* Toggle major site options (uncomment and use true or false (no quotes): */
//override.glassMode          = true;
//override.hideChatHeading    = true;
//override.colorizeStars      = true;
//override.hideWelcome        = true;
override.chatGuildTags      = 3; // 0 = off, 1 = normal, 2 = small, 3 = after name, 4 = float:right
//override.fishyPoop          = false;
//override.glowGuild          = 'TTF'; // set false to turn off, 'TAG' to highlight guild tag

/* Change background image (use url): */
override.bgImage            = '';

/* Customizable colors (use #hex, colorname, transparent, rgba(r,g,b,a): */
override.highlightColor     = '';
override.textColor          = '';
override.chatColor          = '';
override.chatGuildTagColor  = '';
override.starColor          = '';
override.diceColor          = '';
override.headerBGColor      = '';
override.headerTextColor    = '';
override.chatboxBGColor     = '';
override.chatboxTextColor   = '';

/* These override poorly-implemented Bootstrap colors (#hex, colorname, transparent, rgba(r,g,b,a): */
override.btnColor           = '';
override.defaultBtnColor    = '';
override.infoBtnColor       = '';
override.primaryBtnColor    = '';
override.goodBtnColor       = '';
override.badBtnColor        = '';

/*       END EDITING       */
/*   DO NOT MODIFY BELOW   */


// DEFAULTS: THESE WILL BE OVERRIDDEN BY UPDATES
defaults.glassMode          = true;
defaults.hideChatHeading    = true;
defaults.colorizeStars      = true;
defaults.hideWelcome        = true;
defaults.chatGuildTags      = 3;
defaults.fishyPoop          = false;
defaults.glowGuild          = false;
defaults.bgImage            = '/assets/flat-jungle-19e1a27c246937471bb4d77d2c5600228ea8f134ff204cfddd2449d86a145bdc.jpg';
defaults.highlightColor     = '#0c0';
defaults.textColor          = '#ccc';
defaults.chatColor          = '#ccc';
defaults.chatGuildTagColor  = '#999';
defaults.starColor          = '#0c0';
defaults.diceColor          = '#0c0';
defaults.headerBGColor      = '#333';
defaults.headerTextColor    = '#ccc';
defaults.chatboxBGColor     = '#333';
defaults.chatboxTextColor   = '#ccc';
defaults.btnColor           = '#0c0';
defaults.defaultBtnColor    = '#ccc';
defaults.infoBtnColor       = '#5bc0de';
defaults.primaryBtnColor    = '#0c0';
defaults.goodBtnColor       = '#0c0';
defaults.badBtnColor        = '#c00';

try{
    savedCfg = JSON.parse(GM_getValue("chemo"));
}
catch(e){
    console.log("no saved config file");
}
for(var k in defaults) {
    chemo[k] = defaults[k];
    if(savedCfg.hasOwnProperty(k)) chemo[k] = savedCfg[k];
    if(override.hasOwnProperty(k) && override[k]!=='') chemo[k] = override[k];
    if(chemo[k]=='default') chemo[k] = defaults[k];
}
GM_setValue("chemo", JSON.stringify(chemo));

var lastSender = [];
var lastTime = [];
var chatRooms = ['GeneralChat','TradeChat','RecruitmentChat','HelpChat','GuildChat'];
// future: consider using https://stackoverflow.com/questions/16670931/hide-scroll-bar-but-while-still-being-able-to-scroll for chrome hiding scrollbars and allow option to re-instate vh/overflow-y
(function() {
    //global styles
    //reset body height to 100% of window
    addGlobalStyle('html, body { height: 100%; }');
    addGlobalStyle('body>.container-fluid:first-of-type { background-image:url("'+chemo.bgImage+'") !important; min-height:100vh !important; }');
    addGlobalStyle('body>.container-fluid:first-of-type { background-size:cover; }');

    //change colors
    addGlobalStyle('body { color:'+chemo.textColor+' !important; }');
    addGlobalStyle('.btn,.required-ingredient { background-color: '+chemo.btnColor+'; border-color: #333; color:#333}');
    addGlobalStyle('.btn:hover { background-color: '+chemo.btnColor+'; border-color: black; filter: brightness(120%); }');
    addGlobalStyle('.btn-default { background-color:'+chemo.defaultBtnColor+'; color:#333}');
    addGlobalStyle('.btn-default:hover { background-color:'+chemo.defaultBtnColor+';}');
    addGlobalStyle('.required-ingredient { background-color: '+chemo.btnColor+'; border-color: #333; color:#333}');
    addGlobalStyle('.required-ingredient:hover { background-color: '+chemo.btnColor+'; border-color: black; filter: brightness(120%); }');
    addGlobalStyle('.btn-info { background-color:'+chemo.infoBtnColor+' !important;  color: white}');
    addGlobalStyle('.btn-info:hover { background-color:'+chemo.infoBtnColor+' !important;}');
    addGlobalStyle('.btn-primary { background-color:'+chemo.primaryBtnColor+' !important; color: white !important}');
    addGlobalStyle('.btn-primary:hover { background-color:'+chemo.primaryBtnColor+' !important;}');
    addGlobalStyle('.btn-danger { background-color: '+chemo.badBtnColor+' !important; color: white}');
    addGlobalStyle('.btn-danger:hover { background-color: '+chemo.badBtnColor+' !important;}');
    addGlobalStyle('.btn-success { background-color:'+chemo.goodBtnColor+' !important; color: white !important;}');
    addGlobalStyle('.btn-success:hover { background-color:'+chemo.goodBtnColor+' !important;}');
    addGlobalStyle('.fa-star,.fa-star-half-o { color:'+chemo.starColor+';}');
    addGlobalStyle('.bg-success { background-color: '+chemo.highlightColor+';}');
    addGlobalStyle('.tier-2 { color: #6AA84F; }');
    addGlobalStyle('.tier-3 { color: #009FFF; }');
    addGlobalStyle('.tier-4 { color: #F37542; }');
    addGlobalStyle('.tier-5 { color: #B09FF9; }');
    addGlobalStyle('.tier-6 { color: #FF104A; }');
    addGlobalStyle('.tier-7 { color: #FF3D88; }');
    addGlobalStyle('.tier-8 { color: #F1C232; }');
    addGlobalStyle('.tier-9 { color: #00C5EB; }');
    addGlobalStyle('.bonus-enabled { background-color:#0c0 !important; }');

    //set static height
    addGlobalStyle('.navbar-fixed-top { height:50px !important }');
    addGlobalStyle('.navbar-fixed-bottom { height:65px !important }');
    addGlobalStyle('.panel-footer { height:143px }');
    addGlobalStyle('.row:first-of-type { margin-left:0 !important; margin-right:0 !important; }');
    addGlobalStyle('body>.container-fluid:first-of-type { margin-top:0 !important; padding-top:50px !important; padding-left:0 !important; padding-bottom:0 !important; background-size:cover; background-position: center center; background-attachment: fixed; }');

    //modify chat panel
    addGlobalStyle('body>.container-fluid:first-of-type>div.row>div.col-xs-3 { padding-left:0 !important; height:calc(100vh - 275px) !important; position: fixed !important; width:20%; }');
    addGlobalStyle('.main-chat-panel.panel-heading { height:37px !important; }');
    addGlobalStyle('#messages { height:calc(100vh - 275px) !important; }');
    addGlobalStyle('#messages .card-text { color: '+chemo.chatColor+'; overflow-wrap:break-word;}');
    addGlobalStyle('.main-page { margin-left:20% !important; width:80% !important; margin-right:0 !important; padding-right:0 !important; margin-bottom:75px; }');
    addGlobalStyle('.main-chat-panel .form-group>br,.main-chat-panel .form-group>.text-muted {display:none; }');
    addGlobalStyle('.main-chat-panel .panel-footer { height:80px !important; }');
    $('#main-chat-text-area').attr('placeholder','Enter text');
    addGlobalStyle('.main-chat-panel .form-group { margin-bottom:0 !important; }');
    addGlobalStyle('.main-chat-panel .form-control { width:98%; }');
    addGlobalStyle('.chat-tab {background-color: #333 !important; color: #ccc !important; border-radius: 0;}');
    addGlobalStyle('.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {background-color: '+chemo.highlightColor+'; color: black !important;}');
    addGlobalStyle('#messages a.mod { color: #F37542 !important; }');
    addGlobalStyle('#messages a.admin { color: #FF104A !important; }');
    addGlobalStyle('#messages a.patreon { color: #009FFF !important; }');
    addGlobalStyle('.chat-guild-tag { color: '+chemo.chatGuildTagColor+' !important} ');
    addGlobalStyle('.chat-tab.nav.nav-pills li a { padding: 12px 10px !important; font-size: 10px; } ');
    addGlobalStyle('.chat-tab.nav.nav-pills li { float:none; } ');
    addGlobalStyle('.chat-tab.nav.nav-pills { display:flex; flex-flow: row nowrap; justify-content:space-evenly; } ');
    addGlobalStyle('.glow { color: '+chemo.chatColor+'; text-align: center; -webkit-animation: glow 1s ease-in-out infinite alternate; -moz-animation: glow 1s ease-in-out infinite alternate; animation: glow 1s ease-in-out infinite alternate; } @-webkit-keyframes glow { from { text-shadow: 0 0 1px '+chemo.chatColor+', 0 0 2px '+chemo.chatColor+', 0 0 3px '+chemo.highlightColor+', 0 0 4px '+chemo.highlightColor+';  } to { text-shadow: 0 0 2px '+chemo.badBtnColor+', 0 0 3px '+chemo.badBtnColor+', 0 0 4px '+chemo.badBtnColor+'; } } ');

    //footer styles
    addGlobalStyle('.percentage-circle-fill {fill: '+chemo.highlightColor+'}');

    //login styles
    addGlobalStyle('.jumbotron { background-color:rgba(0,0,0,0.75) !important; }');

    //inventory styles
    addGlobalStyle('.inventory-panel { max-height:none !important; overflow-y:visible !important; }'); //margin-bottom: 75px;
    addGlobalStyle('.inventory-panel>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
    addGlobalStyle('.components-panel>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
    addGlobalStyle('.components-panel>.panel-heading>.panel-body { max-height:none !important; overflow-y:visible !important;  }');
    addGlobalStyle('.components-panel>.panel-heading:first-of-type { margin-bottom: 40px;  }');

    //profile styles
    addGlobalStyle('#profile-main-div .col-xs-8 { max-height:none !important; overflow-y:visible !important; margin-bottom:75px;}');
    addGlobalStyle('.progress-bar {background-color:'+chemo.highlightColor+'; color: black !important;}');
    //addGlobalStyle('.main-page>.well-transparent {margin-bottom: 75px; !important; max-height:100vh !important;}');
    addGlobalStyle('#profile-main-div .col-xs-8 .panel-success .panel-body { max-height:none !important; overflow-y:visible !important;}');

    //leaderboard styles
    addGlobalStyle('.leaderboard-panel {overflow-y:visible !important; max-height:none !important; margin-bottom: 75px; }');

    //market styles
    //addGlobalStyle('div.col-xs-9.main-page .panel-success {margin-bottom: 75px;}');
    addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row {display:flex;flex-flow: row wrap;}');
    addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row>div.col-md-2 {height:auto !important;}');

    //crafting styles
    addGlobalStyle('html>body>div.container-fluid>div.row>div.col-xs-9.main-page>div.panel.panel-success>div.panel-body>div.row>div.col-md-3 {height:auto !important;overflow-y:visible !important;}');
    addGlobalStyle('.recipe-requirements {height:auto !important;overflow-y:visible !important;}');
    addGlobalStyle('.ingredients>div.panel-success>div.panel-body {max-height:none !important; height:auto !important; overflow-y:visible !important;}');

    //skillbar styles
    addGlobalStyle('#skill_details .panel-footer {height: auto !important;}');
    addGlobalStyle('#skill_details .panel-body {text-align:center;}');
    addGlobalStyle('#skill_details {width: 450px; max-height:none !important; overflow-y:visible !important;}');
    addGlobalStyle('.dropdown .dropdown-toggle {height:100%;}');
    addGlobalStyle('#dice-roll {width: 300px; margin-left:auto !important; margin-right:auto !important;display:flex;flex-flow:row;justify-content:space-evenly;}');
    addGlobalStyle('#dice-roll div {width: 37px;padding:0 !important;margin:0 !important;color:'+chemo.diceColor+';}');
    addGlobalStyle('#active_skill_dropdown { color: #ccc !important; }');
    addGlobalStyle('.dropdown.active-skill-dropdown.bg-success { background-color: black !important; border-left:1px solid '+chemo.highlightColor+';border-right:1px solid '+chemo.highlightColor+'; }');

    //Dashboard updates
    //addGlobalStyle('div.container-fluid>div.row>div.col-xs-9.main-page>div.well-transparent {overflow-y:visibile !important; max-height:none !important; display:flex; flex-flow: row wrap;justify-content: space-between;}');
    //addGlobalStyle('div.container-fluid>div.row>div.col-xs-9.main-page>div.well-transparent>div.panel.panel-info {width:450px;}');
    //addGlobalStyle('.used-crafting-queue {width:auto;}');

    if(chemo.glassMode) enterGlassMode();

    if(chemo.hideChatHeading) {
        addGlobalStyle('.main-chat-panel>.panel-heading {display:none}');
        addGlobalStyle('#messages { height:calc(100vh - 238px) !important; overflow-y:default !important; }');
    }

    if (chemo.colorizeStars) {
        addGlobalStyle('.quality .fa:nth-of-type(1),.quality .fa:nth-of-type(2),.quality .fa:nth-of-type(3) { color:#0CCC42 !important;}');
        addGlobalStyle('.quality .fa:nth-of-type(4),.quality .fa:nth-of-type(5),.quality .fa:nth-of-type(6) { color:#48f !important;}');
        addGlobalStyle('.quality .fa:nth-of-type(7),.quality .fa:nth-of-type(8),.quality .fa:nth-of-type(9) { color:#b4e !important;}');
        addGlobalStyle('.quality .fa:nth-of-type(10) { color:#D96801 !important;}');
        addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(1),.dropdown-menu li a .fa:nth-of-type(2),.dropdown-menu li a .fa:nth-of-type(3) { color:#0CCC42 !important;}');
        addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(4),.dropdown-menu li a .fa:nth-of-type(5),.dropdown-menu li a .fa:nth-of-type(6) { color:#48f !important;}');
        addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(7),.dropdown-menu li a .fa:nth-of-type(8),.dropdown-menu li a .fa:nth-of-type(9) { color:#b4e !important;}');
        addGlobalStyle('.dropdown-menu li a .fa:nth-of-type(10) { color:#D96801 !important;}');
        addGlobalStyle('.popover .fa:nth-of-type(1),.popover .fa:nth-of-type(2),.popover .fa:nth-of-type(3) { color:#0CCC42 !important;}');
        addGlobalStyle('.popover .fa:nth-of-type(4),.popover .fa:nth-of-type(5),.popover .fa:nth-of-type(6) { color:#48f !important;}');
        addGlobalStyle('.popover .fa:nth-of-type(7),.popover .fa:nth-of-type(8),.popover .fa:nth-of-type(9) { color:#b4e !important;}');
        addGlobalStyle('.popover .fa:nth-of-type(10) { color:#D96801 !important;}');
    }

    if (chemo.hideWelcome){
        addGlobalStyle('.main-page .jumbotron { display:none !important; }');
    }

    addGlobalStyle('#messages .card-text br:first-of-type { display:none');

    //fix chat stamps on page load
    fixAllChatStamps();
    //fix chat stamps on new chat being added
    $('#GeneralChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('GeneralChat'); });
    $('#TradeChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('TradeChat'); });
    $('#RecruitmentChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('RecruitmentChat'); });
    $('#HelpChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('HelpChat'); });
    $('#GuildChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('GuildChat'); });
})();


//when an internal link is called
$( document ).on('turbolinks:load', function() {
    //re-fix chat stamps
    fixAllChatStamps();
    $("#messages").scrollTop($("#messages")[0].scrollHeight);
    //re-bind fixing chat stamps
    $('#GeneralChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('GeneralChat'); });
    $('#TradeChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('TradeChat'); });
    $('#RecruitmentChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('RecruitmentChat'); });
    $('#HelpChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('HelpChat'); });
    $('#GuildChat .messages').bind('DOMNodeInserted', function() { fixChatStamps('GuildChat'); });
});
function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}
function enterGlassMode(){
    addGlobalStyle('body { background-color:none !important; }');
    addGlobalStyle('.navbar-default { background-color:'+chemo.headerBGColor+'; color:'+chemo.headerTextColor+' !important; }');
    addGlobalStyle('.navbar-default .navbar-nav>li>a { color:'+chemo.headerTextColor+' !important }');
    addGlobalStyle('.navbar-default .dropdown-menu>li>a { color:'+chemo.headerTextColor+' !important }');
    addGlobalStyle('.navbar-default .navbar-nav>li>a:hover { color:#ccc !important }');
    addGlobalStyle('.navbar-default .navbar-nav>li>a:focus { color:#ccc !important }');
    addGlobalStyle('.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover { background-color:#111 !important }');
    addGlobalStyle('.percentage-circle-contents { color:#ccc !important }');
    addGlobalStyle('.percentage-circle-contents>img { filter: invert(100%); }');
    addGlobalStyle('.percentage-circle-fg {fill: black;}');
    addGlobalStyle('a { color:'+chemo.highlightColor+' !important; }');

    //panel colors
    addGlobalStyle('.panel, .breadcrumb { background-color: rgba(0,0,0,0.5) !important; }');
    addGlobalStyle('.main-chat-panel .panel-footer { background-color:rgba(0,0,0,0.5) !important; }');
    addGlobalStyle('.panel-heading, .alert-success {color: #ccc !important; background-color: rgba(0,0,0,0.5) !important; border-color: #666 !important;}');
    addGlobalStyle('.panel-success, .panel-default, .panel-info, .panel-primary { border-color: #666;}');
    addGlobalStyle('.panel-footer {color: #ccc; background-color: rgba(0,0,0,0.5); border-color: #666;}');

    addGlobalStyle('.dropdown-menu,.list-group-item,.well { background-color:'+chemo.headerBGColor+'; }');
    addGlobalStyle('.dropdown-menu a:hover { background-color:#111 !important; }');
    addGlobalStyle('.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li>a:hover{ background-color:rgba(0,0,0,0.5) !important; }');
    addGlobalStyle('.table>tbody>tr.success>td, .table>tbody>tr.success>th, .table>tbody>tr>td.success, .table>tbody>tr>th.success, .table>tfoot>tr.success>td, .table>tfoot>tr.success>th, .table>tfoot>tr>td.success, .table>tfoot>tr>th.success, .table>thead>tr.success>td, .table>thead>tr.success>th, .table>thead>tr>td.success, .table>thead>tr>th.success{background-color: rgba(255,255,255,0.25) !important;}');
    addGlobalStyle('select {background-color:#333}');
    addGlobalStyle('.table-striped>tbody>tr:nth-of-type(odd) { background: none !important}');
    addGlobalStyle('.table-hover>tbody>tr:hover { background-color: rgba(0,0,0,0.4) !important}');
    addGlobalStyle('.popover { background-color: #333 !important}');
    addGlobalStyle('.popover-title { background-color: #333 !important}');
    addGlobalStyle('#main-chat-text-area {background-color:'+chemo.chatboxBGColor+' !important;color:'+chemo.chatboxTextColor+' !important}');
    addGlobalStyle('#progressBarContainer {background-color:#ccc !important;border-radius: 4px;-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);}');
    addGlobalStyle('#progressBar {background-color:'+chemo.highlightColor+' !important;}');
    addGlobalStyle('#user-stat-actions td.row-1.column-1,#user-stat-actions td.row-1.column-2{ color: black !important; padding-top:2px !important;}');
    addGlobalStyle('.nav>li>a:hover { background-color: #333;');
    addGlobalStyle('.progress,.input-group-addon,.form-control { background-color: #ccc;');

    addGlobalStyle('.guild-collapsible-ledger-content { background-color: transparent !important; padding:0 !important; }');
    addGlobalStyle('.guild-collapsible-ledger { background-color: rgba(0,0,0,0.5) !important; color:'+chemo.highlightColor+' !important; }');
    addGlobalStyle('.col-md-9.guild-content {  height: auto !important; overflow: visible !important; display:block !important }');
    addGlobalStyle('.panel.guild-member { border-left:0px solid !important; }');
    addGlobalStyle('.col-md-9.guild-content .guild-member {  display:block !important }');
    addGlobalStyle('.guild-left-style { max-height:none !important; overflow-y:visible !important; }');
    addGlobalStyle('.guild-left-style .guild-member { display:block !important; }');

    //modal
    addGlobalStyle('.modal-content{ background-color:#333; }');
    addGlobalStyle('.modal-content input, .modal-content select{ background-color:#ccc !important; color: black }');
    //skills
    addGlobalStyle('#skill_details, #skill_details .panel-default { background-color:rgba(0,0,0,.5) !important; margin-bottom:0;}');

}
function fixAllChatStamps(){
    fixChatStamps(chatRooms[0]);
    fixChatStamps(chatRooms[1]);
    fixChatStamps(chatRooms[2]);
    fixChatStamps(chatRooms[3]);
    fixChatStamps(chatRooms[4]);
}
function fixChatStamps(ch_name){
    $('#'+ch_name+'>.panel>#messages .card-text').each(function(){
        var dt = $(this).children(".chat-date").html();
        var thisSender = $(this).children("span:first:not(.chat-date)").children("a").html();
        var tss;
        try{
            tss = thisSender.split('] ');
            thisSender = tss.pop();
            if(chemo.chatGuildTags == 0) {
            } else if (tss.length>0) {
                var gt = tss.join().substring(1)
                if(gt.codePointAt(0)==9749 && chemo.fishyPoop) {
                    gt = String.fromCodePoint(128169);
                }
                var gl = '';
                if (chemo.glowGuild && chemo.glowGuild==gt) {
                    gl = ' glow';
                }
                if (chemo.chatGuildTags == 1) {
                    thisSender = '<span class="chat-guild-tag'+gl+'">[' + gt + ']</span> ' + thisSender;
                } else if (chemo.chatGuildTags == 2) {
                    thisSender = '<small class="chat-guild-tag'+gl+'">'+ gt + '</small> ' + thisSender;
                } else if (chemo.chatGuildTags == 3) {
                    thisSender = thisSender + ' <small class="chat-guild-tag'+gl+'">'+ gt + '</small>';
                } else if (chemo.chatGuildTags == 4) {
                    thisSender = '<small style="float:right;" class="chat-guild-tag'+gl+'">'+ gt + '</small> ' + thisSender;
                }
            }
        } catch(err) {}
        if (dt.match(/\d\d\/\d\d \d\d:\d\d/)) {
            dt=dt.replace(/\d\d\/\d\d /,'');
            var thisTime = dt;
            if(thisSender == lastSender[ch_name]) {
                $(this).children("span:first:not(.chat-date)").children("a").html("");
            } else if (thisSender == '') {
                $(this).children("span:first:not(.chat-date)").children("a").html("");
            } else {
                $(this).children("span:first:not(.chat-date)").children("a").html(thisSender);
                lastSender[ch_name] = thisSender;
            }

            if(thisTime == lastTime[ch_name]) {
                $(this).children(".chat-date").html("");
            } else if (thisTime == '') {
                $(this).children(".chat-date").html("");
            } else {
                $(this).children(".chat-date").html(thisTime);
                lastTime[ch_name] = thisTime;
            }
        }
    });
}
